import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import {
  breakpoints,
  spacing,
  shape,
  typography,
} from '../tokens';
import { overrides } from './overrides';

const PurpleTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: purple['500'],
    },
    secondary: {
      main: green['500'],
    },
  },
  breakpoints: {
    keys: breakpoints.keys,
    values: breakpoints.values,
  },
  shape: {
    borderRadius: shape.borderRadius,
  },
  typography: {
    h1: {
      fontSize: typography.h1.fontSize,
      fontWeight: typography.h1.fontWeight,
    },
    h2: {
      fontSize: typography.h2.fontSize,
      fontWeight: typography.h2.fontWeight,
    },
    h3: {
      fontSize: typography.h3.fontSize,
      fontWeight: typography.h3.fontWeight,
    },
    h4: {
      fontSize: typography.h4.fontSize,
      fontWeight: typography.h4.fontWeight,
    },
    h5: {
      fontSize: typography.h5.fontSize,
      fontWeight: typography.h5.fontWeight,
    },
    h6: {
      fontSize: typography.h6.fontSize,
      fontWeight: typography.h6.fontWeight,
    },
    subtitle1: {
      fontSize: typography.subtitle1.fontSize,
    },
    subtitle2: {
      fontSize: typography.subtitle2.fontSize,
      fontWeight: typography.subtitle2.fontWeight,
    },
    body1: {
      fontSize: typography.body1.fontSize,
    },
    body2: {
      fontSize: typography.body2.fontSize,
    },
  },
  spacing,
  overrides,
});

export default PurpleTheme;
