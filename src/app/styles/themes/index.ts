import purple from './purple';
import blue from './blue';
import vp from './vp';

export {
  blue,
  purple,
  vp,
};

export default {
  blue,
  purple,
  vp,
};
