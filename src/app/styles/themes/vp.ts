// https://material-ui.com/customization/default-theme/

import { createMuiTheme } from '@material-ui/core/styles';
import {
  shape,
  breakpoints,
  spacing,
  typography,
} from '../tokens';
import { overrides } from './overrides';

const VPTheme = createMuiTheme({
  palette: {
    type: 'light',
    common: {
      black: '#000000',
      white: '#ffffff',
    },
    primary: {
      light: '#677bad',
      main: '#425A99',
      dark: '#2e3e6b',
      contrastText: '#ffffff',
    },
    secondary: {
      light: '#ffe333',
      main: '#FFDC00',
      dark: '#b29a00',
      contrastText: 'rgba(0, 0, 0, 0.87)',
    },
    error: {
      light: '#d48f8f',
      main: '#CA7373',
      dark: '#8d5050',
      contrastText: '#ffffff',
    },
    warning: {
      light: '#d4d28f',
      main: '##CAC773',
      dark: '#8d8b50',
      contrastText: 'rgba(0, 0, 0, 0.87)',
    },
    info: {
      light: '#64b5f6',
      main: '#2196f3',
      dark: '#1976d2',
      contrastText: '#ffffff',
    },
    success: {
      light: '#8bdd9b',
      main: '#6ED583',
      dark: '#4d955b',
      contrastText: 'rgba(0, 0, 0, 0.87)',
    },
    text: {
      primary: '#60657B',
      secondary: '#A1A5BC',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)',
    },
    background: {
      paper: '#fff',
      default: '#fafafa',
    },
  },
  breakpoints: {
    keys: breakpoints.keys,
    values: breakpoints.values,
  },
  shape: {
    borderRadius: shape.borderRadius,
  },
  typography: {
    h1: {
      color: '#4b4e5c',
      fontSize: typography.h1.fontSize,
      fontWeight: typography.h1.fontWeight,
    },
    h2: {
      color: '#4b4e5c',
      fontSize: typography.h2.fontSize,
      fontWeight: typography.h2.fontWeight,
    },
    h3: {
      color: '#4b4e5c',
      fontSize: typography.h3.fontSize,
      fontWeight: typography.h3.fontWeight,
    },
    h4: {
      color: '#4b4e5c',
      fontSize: typography.h4.fontSize,
      fontWeight: typography.h4.fontWeight,
    },
    h5: {
      color: '#4b4e5c',
      fontSize: typography.h5.fontSize,
      fontWeight: typography.h5.fontWeight,
    },
    h6: {
      color: '#4b4e5c',
      fontSize: typography.h6.fontSize,
      fontWeight: typography.h6.fontWeight,
    },
    subtitle1: {
      color: '#60657B',
      fontSize: typography.subtitle1.fontSize,
      fontWeight: typography.subtitle2.fontWeight,
    },
    subtitle2: {
      color: '#4b4e5c',
      fontSize: typography.subtitle2.fontSize,
      fontWeight: typography.subtitle2.fontWeight,
    },
    body1: {
      color: '#60657B',
      fontSize: typography.body1.fontSize,
    },
    body2: {
      color: '#4b4e5c',
      fontSize: typography.body2.fontSize,
    },
  },
  overrides,
  spacing,
});

export default VPTheme;
