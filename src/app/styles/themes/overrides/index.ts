// eslint-disable-next-line
import { Overrides } from '@material-ui/core/styles/overrides';

/**
 * Override aqui será replicado para todos os temas criados que
 * tiverem importado o overrides.
 *
 * Exemplo de override na MuiButton:
 *
 * export const overrides: Overrides = {
 *   MuiButton: {
 *     root: {
 *       padding: 50,
 *     },
 *   },
 * };
 */

export const overrides: Overrides = {};
