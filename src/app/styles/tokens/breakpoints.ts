interface Breakpoints {
  keys: [
    'xs',
    'sm',
    'md',
    'lg',
    'xl'
  ],
  values: {
    xs: number,
    sm: number,
    md: number,
    lg: number,
    xl: number,
  }
}

export const breakpoints: Partial<Breakpoints> = {
  keys: [
    'xs',
    'sm',
    'md',
    'lg',
    'xl',
  ],
  values: {
    xs: 0,
    sm: 600,
    md: 920,
    lg: 1280,
    xl: 1920,
  },
};
