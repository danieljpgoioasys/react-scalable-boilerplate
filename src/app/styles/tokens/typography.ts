export const typography = {
  h1: {
    fontSize: '1.8rem',
    fontWeight: 500,
  },
  h2: {
    fontSize: '1.5rem',
    fontWeight: 500,
  },
  h3: {
    fontSize: '1.45rem',
    fontWeight: 500,
  },
  h4: {
    fontSize: '1.40rem',
    fontWeight: 500,
  },
  h5: {
    fontSize: '1.35rem',
    fontWeight: 500,
  },
  h6: {
    fontSize: '1.30rem',
    fontWeight: 400,
  },
  subtitle1: {
    fontSize: '1rem',
    fontWeight: 500,
  },
  subtitle2: {
    fontSize: '0.9rem',
    fontWeight: 500,
  },
  body1: {
    fontSize: '1rem',
  },
  body2: {
    fontSize: '0.9rem',
  },
};
