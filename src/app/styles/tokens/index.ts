import { breakpoints } from './breakpoints';
import { typography } from './typography';

const shape = {
  borderRadius: 4,
};

const spacing = 8;

export {
  breakpoints,
  shape,
  spacing,
  typography,
};

export default {
  shape,
  breakpoints,
  spacing,
  typography,
};
