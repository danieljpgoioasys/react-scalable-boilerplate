import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body{
    background-color: ${({ theme }) => theme.palette.background.default};
  }
  
  #root{
    height: 100vh;
  }
`;

// h1, h2, h3, h4, h5, h6{
//   margin-block-start: 0;
//   margin-block-end: 0;
// }

/* h1{
    color: ${({ theme }) => theme.typography.h1.color};
    font-size: ${({ theme }) => theme.typography.h1.fontSize};
    font-weight: ${({ theme }) => theme.typography.h1.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h1.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h1.lineHeight};
  }
  h2{
    color: ${({ theme }) => theme.typography.h2.color};
    font-size: ${({ theme }) => theme.typography.h2.fontSize};
    font-weight: ${({ theme }) => theme.typography.h2.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h2.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h2.lineHeight};
  }
  h3{
    color: ${({ theme }) => theme.typography.h3.color};
    font-size: ${({ theme }) => theme.typography.h3.fontSize};
    font-weight: ${({ theme }) => theme.typography.h3.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h3.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h3.lineHeight};
  }
  h4{
    color: ${({ theme }) => theme.typography.h4.color};
    font-size: ${({ theme }) => theme.typography.h4.fontSize};
    font-weight: ${({ theme }) => theme.typography.h4.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h4.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h4.lineHeight};
  }
  h5{
    color: ${({ theme }) => theme.typography.h5.color};
    font-size: ${({ theme }) => theme.typography.h5.fontSize};
    font-weight: ${({ theme }) => theme.typography.h5.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h5.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h5.lineHeight};
  }
  h6{
    color: ${({ theme }) => theme.typography.h6.color};
    font-size: ${({ theme }) => theme.typography.h6.fontSize};
    font-weight: ${({ theme }) => theme.typography.h6.fontWeight};
    letter-spacing: ${({ theme }) => theme.typography.h6.letterSpacing};
    line-height: ${({ theme }) => theme.typography.h6.lineHeight};
  }

  fieldset{
    border-width: 0px;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-block-end: 0px;
    padding-block-start: 0px;
    padding-inline-end: 0px;
    padding-inline-start: 0px;
    border-style: none;
    border-color: unset;
    border-image: none;
  }

  legend{
    padding-inline-start: 0;
    padding-inline-end: 0;
  } */
