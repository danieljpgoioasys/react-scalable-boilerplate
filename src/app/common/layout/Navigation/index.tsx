import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from './Header';
import { Container, Main } from './styles';

const Navigation = () => (
  <Container>
    <Header />
    <Main>
      <Outlet />
    </Main>
  </Container>
);

export default Navigation;
