import styled from 'styled-components';

export const Container = styled.div``;

export const Main = styled.main`
  max-width: 920px;
  margin: 0 auto;
  padding: ${(props) => props.theme.spacing(4, 0)};
  height: calc(100% - 53px);
`;
