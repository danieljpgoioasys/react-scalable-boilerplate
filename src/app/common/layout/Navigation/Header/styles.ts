import styled from 'styled-components';

export const Container = styled.div`
  padding: ${(props) => props.theme.spacing(2, 2)};
  border-bottom: solid 1px;
`;
