import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Button, { ButtonProps } from '.';

export default {
  title: 'Components/Button',
  component: Button,
  args: {},
  argTypes: {
    children: {
      action: 'text',
    },
    onClick: {
      action: 'clicked',
    },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => {
  const { children, ...rest } = args;

  return (
    <Button {...rest}>
      {children}
    </Button>
  );
};

export const Contained = Template.bind({});
Contained.args = {
  variant: 'contained',
  children: 'Button',
  size: 'small',
  color: 'primary',
};

export const Outlined = Template.bind({});
Outlined.args = {
  variant: 'outlined',
  children: 'Button',
  size: 'small',
  color: 'primary',
};

export const Text = Template.bind({});
Text.args = {
  variant: 'text',
  children: 'Button',
  size: 'small',
  color: 'primary',
};
