import React from 'react';
import { Button as ButtonMUI, ButtonProps as ButtonMUIProps } from '@material-ui/core';

export interface ButtonProps extends ButtonMUIProps { }

const Button: React.FC<ButtonProps> = (props) => {
  const { children, ...rest } = props;

  return (
    <ButtonMUI {...rest}>
      {children}
    </ButtonMUI>
  );
};

export default Button;
