export const getLocalStorageData = <T>(key: string): T | null => {
  const data = localStorage.getItem(key);
  return data && JSON.parse(data);
};

export const setLocalStorageData = <T>(key: string, body: T): void => {
  localStorage.setItem(key, JSON.stringify(body));
};

export default {
  getLocalStorageData,
  setLocalStorageData,
};
