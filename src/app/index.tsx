import React from 'react';
import { ThemeProvider as ThemeProviderStyled } from 'styled-components';
import { ThemeProvider as ThemeProviderMUI } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import Routers from './main/routers';
import GlobalStyle from './styles/global';
import { vp } from './styles/themes';
import store from './main/store';

const App: React.FC = () => (
  <ThemeProviderStyled theme={vp}>
    <ThemeProviderMUI theme={vp}>
      <GlobalStyle />
      <Provider store={store}>
        <Routers />
      </Provider>
    </ThemeProviderMUI>
  </ThemeProviderStyled>
);

export default App;
