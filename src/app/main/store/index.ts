import { createStore, Store, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { User } from './reducers/user/types';
import rootReducer from './reducers';

/**
 * Aqui deverá ficar as interfaces de cada reducer "./types",
 * para auxiliar na tipagem dos dados salvos na store
 */

export interface ApplicationState {
  user: User
}

const store: Store<ApplicationState> = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk),
  ),
);

export default store;
