import { combineReducers } from 'redux';
import userReducer from './user/reducer';

/**
 * Aqui deverá ficar as importações de cada reducer,
 * para assim serem combinados no reducer global e serem alocados
 * no store da aplicação.
 */

const rootReducer = combineReducers({
  user: userReducer,
});

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer;
