/**
 * Exemplo de arquivo de reducer,
 *
 * Vantagens desse padrão, fortemente tipado é que o return da action
 * selecioanda no case será exatamente o que era tá esperando na tipagem,
 * lembrando da tipagem no parametro actions do reducer.
 *
 * No exemplo, o UserActions tipando o parametro action e o case selecionado
 * sendo SAVE_USER, faz com que o retorno seja obrigátoriamente: { id, email, name }
 */

import { User, UserActions, types } from './types';

const initialState: User = {
  email: '',
  id: '',
  name: '',
};

const reducer = (state = initialState, actions: UserActions): User => {
  switch (actions.type) {
    case types.SAVE_USER:
      return {
        ...state,
        id: actions.payload.id,
        email: actions.payload.email,
        name: actions.payload.name,
      };

    case types.LOGOUT_USER:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
