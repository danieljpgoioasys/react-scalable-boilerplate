/**
 * Exemplo de arquivo de types,
 *
 * Não esquecer de exportar o tipo das usas Actions, como no exemplo abaixo,
 * isso irá permitir a as actions e o reducer definifir o que deverá retorna e
 * o que deverá ser aceito em cada.
 */

export enum types {
  SAVE_USER = '@user/SAVE_USER',
  LOGOUT_USER = '@user/LOGOUT_USER',
}

export interface User {
  readonly id: string;
  readonly email: string;
  readonly name: string;
}

interface SaveAction {
  type: typeof types.SAVE_USER,
  payload: {
    id: string;
    email: string;
    name: string;
  }
}

interface LogoutAction {
  type: typeof types.LOGOUT_USER,
}

export type UserActions = (SaveAction | LogoutAction)

export default types;
