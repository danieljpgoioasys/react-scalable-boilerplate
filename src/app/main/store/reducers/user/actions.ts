/**
 * Exemplo de arquivo de actions,
 *
 * Vantagens desse padrão, fortemente tipado é que o payload será de
 * acordo com o type especificado, juntamente com a tipagem da função.
 *
 * No exemplo, o UserActions tipando a função e o type sendo SAVE_USER,
 * faz com que o payload seja obrigátoriamente: { id, email, name }
 */

import { UserActions, types } from './types';

const actions = {
  saveUser: (id: string, email: string, name: string): UserActions => ({
    type: types.SAVE_USER,
    payload: {
      id,
      email,
      name,
    },
  }),

  logoutUser: (): UserActions => ({
    type: types.LOGOUT_USER,
  }),
};

export default actions;
