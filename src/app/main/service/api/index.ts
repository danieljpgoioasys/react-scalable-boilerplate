import { apiConfig, interceptor } from './config';
import env from '../env';

export const api = {
  checkout: apiConfig(env.checkout),
  manager: apiConfig(env.manager),
  search: apiConfig(env.search),
  security: apiConfig(env.security),
};

api.checkout.interceptors.request.use(interceptor);
api.manager.interceptors.request.use(interceptor);
api.search.interceptors.request.use(interceptor);
api.security.interceptors.request.use(interceptor);

export default api;
