/* eslint-disable camelcase */

import axios, { AxiosRequestConfig } from 'axios';
import { stringify } from 'querystring';
import { setLocalStorageData, getLocalStorageData } from '../../../common/utils/helpers/localStorage';
import { Token, TokenStorage } from './types';
import api from './index';
import history from '../history';
import env from '../env';

export const apiConfig = (url: string | undefined) => axios
  .create({
    baseURL: url,
  });

const newConfig = (config: AxiosRequestConfig, token: string): AxiosRequestConfig => ({
  ...config,
  headers: {
    ...config.headers,
    common: {
      ...config.headers.common,
      Authorization: `Bearer ${token}`,
    },
  },
});

const getToken = async (
  username?: string,
  password?: string,
  securityClientId?: string,
  securityClientSecret?: string,
  grant_type: string = 'password'): Promise<Token> => {
  const config: AxiosRequestConfig = {
    auth: {
      username: securityClientId || 'empty username',
      password: securityClientSecret || 'empty password',
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  };

  const body = {
    ...(username && { username }),
    ...(password && { password }),
    grant_type,
    scope: 'read write',
  };

  const { data } = await api.security
    .post('oauth/token', stringify(body), config)
    .catch((err) => err);

  return data;
};

const setToken = async (
  key: string,
  today: Date,
  grant_type: string = 'password',
  securityClientSecret?: string,
  securityUsername?: string,
  securityPassword?: string,
  securityClientId?: string,
): Promise<{ access_token: string }> => {
  const { access_token, expires_in } = await getToken(
    securityUsername,
    securityPassword,
    securityClientId,
    securityClientSecret,
    grant_type,
  );

  const expiration_date = new Date(today.getTime() + ((expires_in - 60) * 1000));
  const refresh_token_expiration_date = new Date(today.getTime() + (86300 * 1000));

  setLocalStorageData(key, {
    access_token,
    expiration_date,
    refresh_token_expiration_date,
  });

  return {
    access_token,
  };
};

export const interceptor = async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
  const userToken = getLocalStorageData<TokenStorage>('userToken');
  const appToken = getLocalStorageData<TokenStorage>('appToken');
  const today = new Date();

  // @TODO verificar o refresh do token no api e o front lidar com isso

  if (!config.url?.endsWith('token')) {
    if (userToken) {
      if (today > new Date(userToken.expiration_date)) {
        if (today > new Date(userToken.refresh_token_expiration_date)) {
          history.push('/auth/login');
          throw new Error('Sua seção expirou. Faça login novamente');
        }
        const { access_token } = await setToken(
          'userToken',
          today,
          'refresh_token',
          env.securityClientSecret,
          undefined,
          undefined,
          undefined,
        );
        return newConfig(config, access_token);
      }
      return newConfig(config, userToken.access_token);
    } if (appToken) {
      if (today > new Date(appToken.expiration_date)) {
        if (today > new Date(appToken.refresh_token_expiration_date)) {
          history.push('/auth/login');
          throw new Error('Sua seção expirou. Faça login novamente');
        }
        const { access_token } = await setToken(
          'appToken',
          today,
          'refresh_token',
          env.securityClientSecret,
          undefined,
          undefined,
          undefined,
        );
        return newConfig(config, access_token);
      }
      return newConfig(config, appToken.access_token);
    }

    const { access_token } = await setToken(
      'appToken',
      today,
      'password',
      env.securityClientSecret,
      env.securityUsername,
      env.securityPassword,
      env.securityClientId,
    );
    return newConfig(config, access_token);
  }
  return config;
};
