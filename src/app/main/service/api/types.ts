/* eslint-disable camelcase */

export interface Token {
  access_token: string;
  expires_in: number;
  jti: string;
  name: string;
  refresh_token: string;
  scope: string;
  token_type: string;
}

export interface TokenStorage {
  access_token: string;
  expiration_date: string;
  refresh_token_expiration_date: string;
}
