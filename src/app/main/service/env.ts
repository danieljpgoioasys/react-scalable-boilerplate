const env = {
  checkout: process.env.REACT_APP_API_VP_SEARCH_URL,
  manager: process.env.REACT_APP_API_VP_MANAGER_URL,
  search: process.env.REACT_APP_API_VP_CHECKOUT_URL,
  security: process.env.REACT_APP_API_VP_SECURITY_URL,
  securityClientId: process.env.REACT_APP_CLIENT_ID_SECURITY,
  securityClientSecret: process.env.REACT_APP_CLIENT_SECRET_SECURITY,
  securityUsername: process.env.REACT_APP_USERNAME_SECURITY,
  securityPassword: process.env.REACT_APP_PASSWORD_SECURITY,
};

export default env;
