import React, { lazy } from 'react';
import {
  Route,
  Navigate,
  BrowserRouter,
  Routes,
} from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import SuspenseWrapper from './SuspenseWrapper';

const Auth = lazy(() => import('../../modules/Auth'));
const Login = lazy(() => import('../../modules/Auth/Login'));
const Forgot = lazy(() => import('../../modules/Auth/Forgot'));
const Welcome = lazy(() => import('../../modules/Auth/Welcome'));
const Navigation = lazy(() => import('../../common/layout/Navigation'));
const Dashboard = lazy(() => import('../../modules/Dashboard'));
const Users = lazy(() => import('../../modules/Users'));
const CreateUsers = lazy(() => import('../../modules/Users/Create'));
const ListUsers = lazy(() => import('../../modules/Users/List'));
const NotFound = lazy(() => import('../../modules/NotFound'));
const Detail = lazy(() => import('../../modules/Users/List/Detail'));

const FallbackAuth = () => <div>Loading Auth...</div>;
const FallbackNavigation = () => <div>Loading Navigation...</div>;
const FallbackContent = () => <div>Loading Content...</div>;
const FallbackInternal = () => <div>Loading Internal...</div>;

const Routers = () => (
  <BrowserRouter>
    <Routes>
      <Route
        key="auth"
        path="auth"
        element={(
          <SuspenseWrapper delay={100} fallback={<FallbackAuth />}>
            <Auth />
          </SuspenseWrapper>
        )}
      >
        <Route
          path="/"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackInternal />}>
              <Welcome />
            </SuspenseWrapper>
          )}
        />
        <Route
          key="login"
          path="login"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackInternal />}>
              <Login />
            </SuspenseWrapper>
          )}
        />
        <Route
          key="forgot"
          path="forgot"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackInternal />}>
              <Forgot />
            </SuspenseWrapper>
          )}
        />
        <Route path="/*" element={<Navigate to="login" />} />
      </Route>

      <Route
        key="navigation"
        element={(
          <SuspenseWrapper delay={100} fallback={<FallbackNavigation />}>
            <PrivateRoute>
              <Navigation />
            </PrivateRoute>
          </SuspenseWrapper>
        )}
      >
        <Route
          key="dashboard"
          path="/"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
              <Dashboard />
            </SuspenseWrapper>
          )}
        />
        <Route
          path="/*"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
              <NotFound />
            </SuspenseWrapper>
          )}
        />
        <Route
          key="users"
          path="users"
          element={(
            <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
              <Users />
            </SuspenseWrapper>
          )}
        >
          <Route
            key="users-list"
            path="list"
            element={(
              <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
                <ListUsers />
              </SuspenseWrapper>
            )}
          >
            <Route
              key="users-detail"
              path=":id"
              element={(
                <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
                  <Detail />
                </SuspenseWrapper>
              )}
            />
          </Route>
          <Route
            key="users-create"
            path="create"
            element={(
              <SuspenseWrapper delay={100} fallback={<FallbackContent />}>
                <CreateUsers />
              </SuspenseWrapper>
            )}
          />
        </Route>
      </Route>

    </Routes>

  </BrowserRouter>
);

export default Routers;
