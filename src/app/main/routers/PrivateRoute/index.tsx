import React from 'react';
import { Navigate } from 'react-router-dom';
import { TokenStorage } from '../../service/api/types';
import { getLocalStorageData } from '../../../common/utils/helpers/localStorage';
import { Children } from '../../../common/types/children';

interface Props extends Children { }

const PrivateRoute = (props: Props) => {
  const { children } = props;

  const userToken = getLocalStorageData<TokenStorage>('userToken');

  return (
    <>
      {userToken && userToken.access_token
        ? children
        : <Navigate to="auth/login" />}
    </>
  );
};

export default PrivateRoute;
