import React, { useState, useEffect } from 'react';

interface Props {
  /**
   * Componente que será exibido após o tempo de `delay` ocorrer.
   */
  element: React.ReactNode;
  /**
   * Tempo máximo de carregamento antes de exibir um loading.
   */
  delay?: number;
}

const Fallback = (props: Props) => {
  const {
    delay = 300,
    element,
  } = props;
  const [show, setShow] = useState(false);

  useEffect(() => {
    const timeout = setTimeout(() => setShow(true), delay);
    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return (
    <>
      {show && element}
    </>
  );
};

export default Fallback;
