import React, { Suspense } from 'react';
import { Children } from '../../../common/types/children';
import Fallback from '../Fallback';

interface Props extends Children {
  /**
   * Componente que será exibido após o tempo de `delay` ocorrer.
   */
  fallback: React.ReactNode;
  /**
   * Tempo máximo de carregamento antes de exibir o `fallback`.
   */
  delay?: number;
}

const SuspenseWrapper = (props: Props) => {
  const {
    fallback,
    delay = 300,
    children,
  } = props;

  return (
    <Suspense
      key="suspense"
      fallback={<Fallback delay={delay} element={fallback} />}
    >
      {children}
    </Suspense>
  );
};

export default SuspenseWrapper;
