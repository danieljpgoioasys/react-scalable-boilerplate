import React from 'react';
import { useNavigate, Outlet } from 'react-router-dom';
import { Container } from './styles';

const Users = () => {
  const navigate = useNavigate();

  return (
    <Container>
      <span>Users Page</span>
      <button
        type="button"
        onClick={() => navigate('/users/create')}
      >
        Create Users Page
      </button>
      <button
        type="button"
        onClick={() => navigate('/users/list')}
      >
        List Users Page
      </button>

      <div>
        <Outlet />
      </div>
    </Container>
  );
};

export default Users;
