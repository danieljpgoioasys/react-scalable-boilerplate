import React from 'react';
import { Outlet } from 'react-router-dom';
import { Container } from './styles';

// request list usuarios
const List = () => (
  <Container>
    List Users Page

    <Outlet />
  </Container>
);

export default List;
