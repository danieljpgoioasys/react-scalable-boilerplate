import styled from 'styled-components';

export const Container = styled.div`
  padding: ${(props) => props.theme.spacing(2)}px;
  border: solid 1px;
  border-radius: ${(props) => props.theme.shape.borderRadius}px;
`;
