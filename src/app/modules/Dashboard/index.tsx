import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Container } from './styles';

const Dashboard = () => {
  const navigate = useNavigate();

  return (
    <Container>
      <button
        type="button"
        onClick={() => navigate('/auth/login')}
      >
        Login
      </button>
      <button
        type="button"
        onClick={() => navigate('/users')}
      >
        Users
      </button>
    </Container>
  );
};

export default Dashboard;
