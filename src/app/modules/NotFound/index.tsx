import React from 'react';
import { useLocation } from 'react-router-dom';
import { Container } from './styles';

const NotFound = () => {
  const location = useLocation();

  return (
    <Container>
      <span>Nenhuma rota encontrada para </span>
      <code>{location.pathname}</code>
    </Container>
  );
};
export default NotFound;
