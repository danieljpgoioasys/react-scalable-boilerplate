import React from 'react';
import { useNavigate } from 'react-router-dom';

const Forgot = () => {
  const navigate = useNavigate();

  return (
    <>
      <button
        type="button"
        onClick={() => navigate('/auth/login')}
      >
        login
      </button>
    </>
  );
};

export default Forgot;
