import React from 'react';
import { useNavigate } from 'react-router-dom';

const Welcome = () => {
  const navigate = useNavigate();

  return (
    <>
      <p>Welcome</p>

      <button
        type="button"
        onClick={() => navigate('/auth/login')}
      >
        login
      </button>
    </>
  );
};

export default Welcome;
