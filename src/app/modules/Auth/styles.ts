import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Container = styled.div`
  border: solid 1px;
  border-radius: ${(props) => props.theme.shape.borderRadius}px;
  padding: ${(props) => props.theme.spacing(2, 2)};
  display: flex;
  justify-content: center;
  width: 100%;
  height: 100%;
  max-height: 200px;
  max-width: 200px;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;
