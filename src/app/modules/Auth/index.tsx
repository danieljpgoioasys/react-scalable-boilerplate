import React from 'react';
import { Outlet } from 'react-router-dom';
import { Wrapper, Container, Content } from './styles';

const Auth = () => (
  <Wrapper>
    <div>
      Auth Page
    </div>

    <Container>
      <Content>
        <Outlet />
      </Content>
    </Container>
  </Wrapper>
);

export default Auth;
