import React from 'react';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const navigate = useNavigate();

  return (
    <>
      <button
        type="button"
        onClick={() => navigate('/')}
      >
        Dashboard
      </button>
      <button
        type="button"
        onClick={() => navigate('/auth/forgot')}
      >
        Forgot
      </button>
    </>
  );
};

export default Login;
