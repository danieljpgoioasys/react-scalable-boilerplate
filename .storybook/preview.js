import React from 'react';
import { ThemeProvider as ThemeProviderStyled } from 'styled-components';
import { ThemeProvider as ThemeProviderMUI } from '@material-ui/core/styles';
import GlobalStyle from '../src/app/styles/global';
import { vp } from '../src/app/styles/themes';
import { CssBaseline } from '@material-ui/core';

const withThemeProvider = (Story, context) => {

  return (
    <>
      <CssBaseline />
      <ThemeProviderStyled theme={vp}>
        <ThemeProviderMUI theme={vp}>
          <GlobalStyle />
          <Story {...context} />
        </ThemeProviderMUI>
      </ThemeProviderStyled >
    </>
  );
}

export const decorators = [withThemeProvider];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};