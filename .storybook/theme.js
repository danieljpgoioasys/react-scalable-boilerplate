import { create } from '@storybook/theming/create';
import logo from '../src/app/common/resources/assets/cropped-logo.png'

export default create({
  brandTitle: 'vp-bolierplate-react',
  brandImage: logo,
});